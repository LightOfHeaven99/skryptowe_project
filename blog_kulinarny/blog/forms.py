from django import forms
from .models import Article

class PostArticle(forms.ModelForm):

    class Meta:
        model = Article
        fields = ('author','title','category','text','ingredients','recipe','article_picture',)