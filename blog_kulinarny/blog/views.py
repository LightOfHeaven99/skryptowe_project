import os
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Article
from .forms import PostArticle
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template
from xhtml2pdf import pisa 
from django.forms.models import model_to_dict


def article_list(request):
    articles = Article.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'blog/article_list.html', {'articles': articles})

def article_list_filtered(request, category):
    articles = Article.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    articles = Article.objects.filter(category=category)
    return render(request, 'blog/article_list_filtered.html', {'articles': articles})

def article_detail(request, pk):
    article = get_object_or_404(Article, pk=pk)
    article.ingredients = article.convert_ingredients_to_array()
    return render(request, 'blog/article_detail.html', {'article': article})

@login_required
def post_new_article(request):
    if request.method == "POST":
        form = PostArticle(request.POST, request.FILES)
        if form.is_valid():
            article = form.save(commit=False)
            article.published_date = timezone.now()
            article.save()
            return redirect('article_detail', pk=article.pk)
    else:
        form = PostArticle()
    return render(request, 'blog/article_edit.html', {'form': form})

@login_required
def article_edit(request, pk):
    article = get_object_or_404(Article, pk=pk)
    if request.method == "POST":
        form = PostArticle(request.POST, request.FILES, instance=article)
        if form.is_valid():
            article = form.save(commit=False)
            article.published_date = timezone.now()
            article.save()
            return redirect('article_detail', pk=article.pk)
    else:
        form = PostArticle(instance=article)
    return render(request, 'blog/article_edit.html', {'form': form})

@login_required
def article_remove(request, pk):
    article = get_object_or_404(Article, pk=pk)
    article.delete()
    return redirect('article_list')

def generate_PDF(request, pk):
    article = get_object_or_404(Article, pk=pk)

    context = {
        'title' : article.title,
        'published_date' : article.published_date,
        'text' : article.text,
        'author' : article.author,
        'ingredients' : article.convert_ingredients_to_array(),
        'recipe' : article.recipe,
    }

    template = get_template('blog/article_detail_pdf.html')
    html  = template.render(context)

    module_dir = os.path.dirname(__file__) 
    file_path = os.path.join(module_dir, 'static/pdf/test.pdf')

    file = open(file_path, "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=file)

    file.seek(0)
    pdf = file.read()
    file.close()            
    return HttpResponse(pdf, 'application/pdf')
