from django.urls import path
from . import views

urlpatterns = [
    path('', views.article_list, name='article_list'),
    path('category/<str:category>/', views.article_list_filtered, name='article_list_filtered'),
    path('article/<int:pk>/', views.article_detail, name='article_detail'),
    path('article/new/', views.post_new_article, name='post_new_article'),
    path('article/<int:pk>/edit/', views.article_edit, name='article_edit'),
    path('article/<int:pk>/remove/', views.article_remove, name='article_remove'),
    path('article/<int:pk>/pdf/', views.generate_PDF, name='generate_PDF'),
]
