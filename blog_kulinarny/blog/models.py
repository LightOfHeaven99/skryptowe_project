from django.db import models
from django.utils import timezone

class Article(models.Model):
    CATEGORY_CHOICES = (
        ('S', 'Śniadanie'),
        ('O', 'Obiad'),
        ('K', 'Kolacja'),
        ('L', 'Lunch'),
        ('D', 'Deser'),
    )

    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    category = models.CharField(max_length=1, choices=CATEGORY_CHOICES, default='S')
    create_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    text = models.TextField()
    ingredients = models.TextField(null=True)
    recipe = models.TextField(null=True)
    article_picture = models.ImageField(upload_to='images/')

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def convert_ingredients_to_array(self):
        ingredients_list = self.ingredients.split(",")
        return ingredients_list

    def __str__(self):
        return self.title 
